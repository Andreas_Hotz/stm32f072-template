/**
 * @file
 *
 * @brief Utility functions for string conversions.
 *
 * Details
 * @author: deexschs
 *
 */

#ifndef _UTIL_CONVERSIONS_H
#define _UTIL_CONVERSIONS_H

#include <sys/types.h>
#include <stdint.h>

#define P_UNUSED        __attribute__((__unused__))

void    util_asctohex(char a, char *s);
uint8_t util_asc2i(char sign);
char*   util_itoa(int value, char* result, int base);
int     util_atoi(const char *nptr);
int     util_atox(const char *str);
void    util_shift_buf_left(char* buf, size_t size);
#endif
